@extends('layouts.app')
@section('title') {{ $title }} @endsection
@section('script')
    <script src="{{ asset($script) }}"></script>
@endsection
@section('content')

 <!-- Content Header (Page header) -->
 <section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ $title }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i>&nbsp;Halaman Utama</a></li>
                    <li class="breadcrumb-item"><a href="#">Referensi</a></li>
                    <li class="breadcrumb-item"><a href="{{ route($route.".index") }}">{{ $title }}</a></li>
                    <li class="breadcrumb-item active">{{ is_null($model->id) ? "Tambah" : "Edit" }}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-edit"></i>&nbsp;Form {{ $title }}
                        </h3>
                    </div>
                    <form method="POST" id="form-submit" class="form-horizontal" action="{{ is_null($model->id) ? route($route.".store") : route($route.".update", array("id"=> $model->id)) }}" autocomplete="off">
                        {{ csrf_field() }}
                        {{ !is_null($model->id) ? method_field('PATCH') : null  }}
                        <div class="card-body">

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Judul <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input type="text" id="title" name="title" value="{{ $model->title ? $model->title : old('title') }}" required="required" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}">
                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Tanggal Mulai<span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input type="text" id="date_first" name="date_first" value="{{ $model->date_first ? $model->date_first : old('date_first') }}" required="required" class="form-control  input-datepicker {{ $errors->has('date_first') ? ' is-invalid' : '' }}">
                                    @if ($errors->has('date_first'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('date_first') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Tanggal Selesai<span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input type="text" id="date_last" name="date_last" value="{{ $model->date_last ? $model->date_last : old('date_last') }}" required="required" class="form-control  input-datepicker {{ $errors->has('date_last') ? ' is-invalid' : '' }}">
                                    @if ($errors->has('date_last'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('date_last') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Deskripsi Penugasan <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <textarea name="body" id="body" class="input-tinymce">{{ $model->body ? $model->body : old('body') }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Prioritas <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input type="number" min="1" id="priority" name="priority" value="{{ $model->priority ? $model->priority : old('priority') }}" required="required" class="form-control {{ $errors->has('priority') ? ' is-invalid' : '' }}">
                                    @if ($errors->has('priority'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('priority') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Status <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <select class="select2 form-control" name="status" id="status" data-placeholder="Pilih Jenis Kelamin">
                                        <option selected disabled></option>
                                        <option {{ (int) $model->status == 0 ? 'selected' : '' }} value="0">Memulai</option>
                                        <option {{ (int) $model->status == 1 ? 'selected' : '' }} value="1">Proses</option>
                                        <option {{ (int) $model->status == 2 ? 'selected' : '' }} value="2">Selesai</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <a href="{{ route($route.".index") }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Kembali ke daftar">
                                                <i class="fas fa-arrow-left"></i>&nbsp;Kembali
                                            </a>
                                        </div>
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-primary" data-toggle='tooltip' data-placement='top' data-title='Simpan perubahan pada form.'>
                                                <i class="fas fa-save"></i>&nbsp;Simpan Perubahan
                                            </button>
                                            <button type="reset" class="btn btn-danger" data-toggle='tooltip' data-placement='top' data-title='Reset isian form.'>
                                                <i class="fas fa-sync"></i>&nbsp;Reset Form
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection