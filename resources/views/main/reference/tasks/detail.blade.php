@extends('layouts.app')
@section('title') {{ $title }} @endsection
@section('content')

 <!-- Content Header (Page header) -->
 <section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ $title }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i>&nbsp;Halaman Utama</a></li>
                    <li class="breadcrumb-item"><a href="#">Referensi</a></li>
                    <li class="breadcrumb-item"><a href="{{ route($route.".index") }}">{{ $title }}</a></li>
                    <li class="breadcrumb-item active">Detail</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <div class="container-fluid">
        @include('layouts.alert')
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-search"></i>&nbsp;Detail {{ $title }}
                        </h3>
                    </div>
                    <div class="card-body">
                        <a href="{{ route($route.".index") }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Kembali ke daftar">
                            <i class="fas fa-arrow-left"></i>&nbsp;Kembali
                        </a>
                        @can('add_'.$route)
                            <a href="{{ route($route.".create") }}" class="btn btn-success" data-toggle='tooltip' data-placement='top' data-title='Tambah data baru'>
                                <i class="fa fa-plus"></i>&nbsp;Tambah Data
                            </a>
                        @endcan
                        @can('edit_'.$route)
                        <a href="{{ route($route.".edit", array('id'=> $model->id)) }}" class="btn btn-warning" data-toggle='tooltip' data-placement='top' data-title='Edit Data'>
                            <i class="fa fa-edit"></i>&nbsp;Edit Data
                        </a>
                        @endcan
                        @can('delete_'.$route)
                        <a href="{{ route($route.".destroy", array('id'=> $model->id)) }}" id="btn-delete" data-redirect="{{ route($route.".index") }}" data-id="{{ $model->id }}" class="btn btn-danger" data-toggle='tooltip' data-placement='top' data-title='Delete Data'>
                            <i class="fa fa-trash"></i>&nbsp;Hapus Data
                        </a>
                        @endcan
                        <h1></h1>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <td width="170">Judul</td>
                                        <td width="10">:</td>
                                        <td>{{ $model->title }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Mulai</td>
                                        <td>:</td>
                                        <td>{{ $model->date_first }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Selesai</td>
                                        <td>:</td>
                                        <td>{{ $model->date_last }}</td>
                                    </tr>
                                    <tr>
                                        <td>Prioritas</td>
                                        <td>:</td>
                                        <td>{{ $model->date_last }}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>:</td>
                                        @php 
                                            $status_name = null;
                                            if((int) $model->status == 0){
                                                $status_name = "<badge class='badge badge-info'>Memulai</badge>";
                                            }else if((int) $model->status == 1){
                                                $status_name = "<badge class='badge badge-warning'>Proses</badge>";
                                            }else if((int) $model->status == 2){
                                                $status_name = "<badge class='badge badge-success'>Selesai</badge>";
                                            }else{
                                                $status_name = "<badge class='badge badge-danger'>Undefined</badge>";
                                            }
                                        @endphp
                                        <td>{!! $status_name !!}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        {!! $model->body !!}
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection