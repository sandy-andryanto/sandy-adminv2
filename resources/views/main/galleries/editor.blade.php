<!DOCTYPE html>
<html>
    <head>
        <title>Editor</title>
        <meta name="base-url" content="{{ url('') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ asset('packages/barryvdh/elfinder/css/elfinder.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/elfinder/css/theme-gray.min.css') }}">
        <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('packages/barryvdh/elfinder/js/elfinder.min.js') }}"></script>
        <script src="{{ asset("packages/barryvdh/elfinder/js/i18n/elfinder.id.js") }}"></script>
    </head>
<body>
    <input type="hidden" id="elfinder_url" value="{{ route("elfinder.connector") }}" />
    <div id="elfinder"></div>
    <script>
        var BASE_URL = "{{ url('') }}";

        function getUrlParam(paramName) {
            var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
            var match = window.location.search.match(reParam) ;

            return (match && match.length > 1) ? match[1] : '' ;
        }

        
        $(document).ready(function(){
            $('#elfinder').elfinder({
                lang : "id",
                url: $("#elfinder_url").val(),
                soundPath: BASE_URL+"/packages/barryvdh/elfinder/sounds",
                customHeaders : {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                height:  $(window).height() - 20,
                getFileCallback : function(file) {
                    window.parent.postMessage({ mceAction: 'fileSelected', data: { file: file } }, '*');
                }
            }).elfinder('instance');
        });
    </script>
</body>
</html>