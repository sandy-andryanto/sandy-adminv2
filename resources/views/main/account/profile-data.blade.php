<!-- Default box -->
<div class="card card-secondary">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>&nbsp;Form Profil
        </h3>
    </div>
    <form method="POST" id="form-submit" class="form-horizontal" action="{{ route('profile.update') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Nama Lengkap <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                    <input type="text" id="name" name="name" required="required" value="{{ $user->name }}" class="form-control  {{ $errors->has('old_password') ? ' is-invalid' : '' }}">
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                <div class="col-sm-10">
                    <select class="select2 form-control" name="gender" id="gender" data-placeholder="Pilih Jenis Kelamin">
                        <option selected disabled></option>
                        <option {{ (int) $user->gender == 1 ? 'selected' : '' }} value="1">Pria</option>
                        <option {{ (int) $user->gender == 2 ? 'selected' : '' }} value="2">Wanita</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Username <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                    <input type="text" id="username" name="username" required="required" value="{{ $user->username }}" class="form-control  {{ $errors->has('username') ? ' is-invalid' : '' }}">
                    @if ($errors->has('username'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Email <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                    <input type="email" id="email" name="email" required="required" value="{{ $user->email }}" class="form-control  {{ $errors->has('email') ? ' is-invalid' : '' }}">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Telepon </label>
                <div class="col-sm-10">
                    <input type="text" id="phone" name="phone"  value="{{ $user->phone }}" class="form-control  {{ $errors->has('phone') ? ' is-invalid' : '' }}">
                    @if ($errors->has('phone'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Kabupaten / Kota </label>
                <div class="col-sm-10">
                    <input type="text" id="city" name="city" required="required" value="{{ $user->city }}" class="form-control  {{ $errors->has('city') ? ' is-invalid' : '' }}">
                    @if ($errors->has('city'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('city') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Alamat Lengkap</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="address" rows="5" id="address">{{ $user->address }}</textarea>
                    @if ($errors->has('address'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label"></label>
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary" data-toggle='tooltip' data-placement='top' data-title='Simpan perubahan pada form.'>
                        <i class="fas fa-save"></i>&nbsp;Simpan Perubahan
                    </button>
                    <button type="reset" class="btn btn-danger" data-toggle='tooltip' data-placement='top' data-title='Reset isian form.'>
                        <i class="fas fa-sync"></i>&nbsp;Reset Form
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /.card -->