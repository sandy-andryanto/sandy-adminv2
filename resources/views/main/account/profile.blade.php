@extends('layouts.app')
@section('title') Profil Saya @endsection
@section('content')

 <!-- Content Header (Page header) -->
 <section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Profil Saya</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i>&nbsp;Halaman Utama</a></li>
                    <li class="breadcrumb-item"><a href="#">Akun Pengguna</a></li>
                    <li class="breadcrumb-item active">Profil Saya</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <div class="container-fluid">
        @include('layouts.alert')
        <div class="row">
            <div class="col-2">
                @include('main.account.profile-image')
            </div>
            <div class="col-10">
                @include('main.account.profile-data')
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection