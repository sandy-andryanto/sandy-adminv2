<div id="crop-avatar">
    <!-- Default box -->
    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fas fa-image"></i>&nbsp;Foto Profil
            </h3>
        </div>
        <div class="card-body">
            <div class="avatar avatar-view text-center">
                <img class="img-responsive img-circle img-thumbnail rounded-circle" src="{{ UserHelper::getRealPhoto() }}"  alt="User profile picture">
                <h1></h1>
                <a href="javascript:void(0);" class="btn btn-sm btn-primary w-100" data-toggle="tooltip" data-placement="top" title="Upload Foto">
                    <i class="fas fa-upload"></i>&nbsp;Ubah Foto Profil
                </a>
            </div>
        </div>
    </div>
    <!-- /.card -->

    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fas fa-newspaper"></i>&nbsp;Informasi Lainnya
            </h3>
        </div>
        <div class="card-body">
            <form>
                <div class="mb-3">
                    <label class="form-label">Bergabung Sejak</label>
                    <input type="text" class="form-control"  value="{{ UserHelper::getJoinDate() }}" disabled>
                </div>
                <div class="mb-3">
                    <label class="form-label">Login Terakhir</label>
                    <input type="text" class="form-control"  value="{{ $user->updated_at }}"  disabled>
                </div>
                <div class="mb-3">
                    <label class="form-label">Alamat IP</label>
                    <input type="text" class="form-control"  value="{{ $ip_address }}"  disabled>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="avatar-modal" aria-labelledby="avatar-modal-label">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fas fa-image"></i>&nbsp;Upload Foto Profil
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('api.user.update.image') }}" class="avatar-form"
                        id="avatar-form" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="avatar-body">
                            <!-- Upload image and data -->
                            <div class="avatar-upload text-left">
                                <input type="hidden" class="avatar-src" name="avatar_src">
                                <input type="hidden" class="avatar-data" name="avatar_data">
                                <input type="file" class="avatar-input file-input" id="avatarInput"  name="avatar_file" accept="image/x-png,image/gif,image/jpeg">
                            </div>
                            <!-- Crop and preview -->

                            <div class="row">
                                <div class="col-md-9">
                                    <div class="avatar-wrapper"></div>
                                </div>
                                <div class="col-md-3">
                                    <div class="avatar-preview preview-lg"></div>
                                    <div class="avatar-preview preview-md"></div>
                                    <div class="avatar-preview preview-sm"></div>
                                </div>
                            </div>
                            <h1></h1>
                            <div class="row avatar-btns">
                                <div class="col-md-9 text-left">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary btn-sm" data-method="rotate"
                                            data-option="-90" title="Rotate -90 degrees">Putar
                                            Kiri</button>
                                        <button type="button" class="btn btn-primary btn-sm" data-method="rotate"
                                            data-option="-15">-15deg</button>
                                        <button type="button" class="btn btn-primary btn-sm" data-method="rotate"
                                            data-option="-30">-30deg</button>
                                        <button type="button" class="btn btn-primary btn-sm" data-method="rotate"
                                            data-option="-45">-45deg</button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary btn-sm" data-method="rotate"
                                            data-option="90" title="Rotate 90 degrees">Putar
                                            Kanan</button>
                                        <button type="button" class="btn btn-primary btn-sm" data-method="rotate"
                                            data-option="15">15deg</button>
                                        <button type="button" class="btn btn-primary btn-sm" data-method="rotate"
                                            data-option="30">30deg</button>
                                        <button type="button" class="btn btn-primary btn-sm" data-method="rotate"
                                            data-option="45">45deg</button>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-primary w-100 avatar-save btn-sm">
                                        <i class="fa fa-check"></i>&nbsp;Selesai dan Simpan
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>

</div>