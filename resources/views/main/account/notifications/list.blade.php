@extends('layouts.app')
@section('title') Pemberitahuan @endsection
@section('script')
    <script src="{{ asset($script) }}"></script>
@endsection
@section('content')

 <!-- Content Header (Page header) -->
 <section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Pemberitahuan</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i>&nbsp;Halaman Utama</a></li>
                    <li class="breadcrumb-item"><a href="#">Akun Pengguna</a></li>
                    <li class="breadcrumb-item active">Pemberitahuan</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-table"></i>&nbsp;Daftar Pemberitahuan
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="table-data" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="20" class="text-center">No</th>
                                        <th>Subjek</th>
                                        <th>Isi Pesan</th>
                                        <th>Status</th>
                                        <th class="text-center" width="150">Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection