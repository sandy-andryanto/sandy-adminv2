@extends('layouts.app')
@section('title') Pemberitahuan @endsection
@section('content')

 <!-- Content Header (Page header) -->
 <section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Pemberitahuan</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i>&nbsp;Halaman Utama</a></li>
                    <li class="breadcrumb-item"><a href="#">Akun Pengguna</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('notifications.index') }}">Pemberitahuan</a></li>
                    <li class="breadcrumb-item active">Detail</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-search"></i>&nbsp;Detail Pemberitahuan
                        </h3>
                    </div>
                    <div class="card-body">
                        <a href="{{ route($route.".index") }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Kembali ke daftar">
                            <i class="fas fa-arrow-left"></i>&nbsp;Kembali
                        </a>
                        <a href="{{ route($route.".destroy", array('id'=> $model->id)) }}" id="btn-delete" data-redirect="{{ route($route.".index") }}" data-id="{{ $model->id }}" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Data">
                            <i class="fas fa-trash"></i>&nbsp;Hapus
                        </a>
                        <h1></h1>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <td  width="180">Subjek</td>
                                        <td>:</td>
                                        <td>{{ $model->subject }}</td>
                                    </tr>
                                    <tr>
                                        <td>Konten Singkat</td>
                                        <td>:</td>
                                        <td>{{ $model->sort_content }}</td>
                                    </tr>
                                    <tr>
                                        <td>Isi Konten Utama</td>
                                        <td>:</td>
                                        <td>{{ $model->content }}</td>
                                    </tr>
                                    <tr>
                                        <td>Dibaca Pada</td>
                                        <td>:</td>
                                        <td>{{ $model->readed_at }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection