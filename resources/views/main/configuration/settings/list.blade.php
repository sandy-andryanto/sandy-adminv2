@extends('layouts.app')
@section('title') {{ $title }} @endsection
@section('script')
    <script src="{{ asset($script) }}"></script>
@endsection
@section('content')

 <!-- Content Header (Page header) -->
 <section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ $title }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i>&nbsp;Halaman Utama</a></li>
                    <li class="breadcrumb-item"><a href="#">Pengaturan</a></li>
                    <li class="breadcrumb-item active">{{ $title }}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <div class="container-fluid">
        @include('layouts.alert')
        <div class="row">
            <div class="col-3">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-cogs"></i>&nbsp;Artisan
                        </h3>
                    </div>
                    <div class="card-body">
                        <a href="javascript:void(0);" data-value="config:cache" class="btn btn-default btn-block uppercase btn-artisan">
                            config cache
                        </a>
                        <a href="javascript:void(0);" data-value="cache:clear" class="btn btn-default btn-block uppercase btn-artisan">
                            cache clear
                        </a>
                        <a href="javascript:void(0);" data-value="config:cache" class="btn btn-default btn-block uppercase btn-artisan">
                            config clear
                        </a>
                        <a href="javascript:void(0);" data-value="route:scan" class="btn btn-default btn-block uppercase btn-artisan">
                            route scan
                        </a>
                        <a href="javascript:void(0);" data-value="key:generate" class="btn btn-default btn-block uppercase btn-artisan">
                            key generate
                        </a>
                        <a href="javascript:void(0);" data-value="model:scan" class="btn btn-default btn-block uppercase btn-artisan">
                            model scan
                        </a>
                        <a href="javascript:void(0);" data-value="permission:cache-reset" class="btn btn-default btn-block uppercase btn-artisan">
                            permission cache reset
                        </a>
                        <a href="javascript:void(0);" data-value="view:clear" class="btn btn-default btn-block uppercase btn-artisan">
                            view clear
                        </a>
                        <a href="javascript:void(0);" data-value="auth:route-permission" class="btn btn-default btn-block uppercase btn-artisan">
                            auth route permission
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-9">
                <!-- Default box -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-edit"></i>&nbsp;Form {{ $title }}
                        </h3>
                    </div>
                    <form method="POST" id="form-submit" enctype="multipart/form-data" class="form-horizontal" action="{{ route('settings.store') }}" autocomplete="off">
                        {{ csrf_field() }}
                        <div class="card-body">

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama Website </label>
                                <div class="col-sm-10">
                                    <input type="text" id="website-name" name="website-name"  value="{{ ConfigHelper::getValueByKey('website-name') }}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Mata Uang</label>
                                <div class="col-sm-10">
                                    <select class="select2 form-control" name="currency-code" id="currency-code" data-placeholder="--Pilih Mata Uang --">
                                        <option></option>
                                        @foreach($currencies as $row => $key)
                                            @php 
                                                $selected_code = ConfigHelper::getValueByKey('currency-code');
                                                $selected = $selected_code ==  $row ? "selected" : "";
                                            @endphp
                                            <option value="{{ $row }}" {{ $selected }}>{{ $row }} ( {{ $key }} ) </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Fav Icon</label>
                                <div class="col-sm-10">
                                    <input type="file" name="favicon" class="file-input" />
                                    @if(ConfigHelper::getValueByKey('favicon') != 'assets/app/img/logo.png')
                                        <h1></h1>
                                        <img src="{{ url(ConfigHelper::getValueByKey('favicon') ) }}" class="img-thumbnail img-fluid" width="300" />
                                    @endif  
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Zona Waktu</label>
                                <div class="col-sm-10">
                                    <select class="select2 form-control" name="timezone" id="timezone" data-placeholder="--Pilih Zona Waktu --">
                                        <option></option>
                                        @foreach($timezones as $row => $key)
                                            @php 
                                                $selected_code = ConfigHelper::getValueByKey('timezone');
                                                $selected = $selected_code ==  $row ? "selected" : "";
                                            @endphp
                                            <option value="{{ $row }}" {{ $selected }}>{{ $row }} | {{ $key }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Mail Driver</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="mail-driver"  value="{{ ConfigHelper::getValueByKey('mail-driver') }}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Mail Host</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="mail-host"  value="{{ ConfigHelper::getValueByKey('mail-host') }}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Mail Port</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="mail-port"  value="{{ ConfigHelper::getValueByKey('mail-port') }}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Mail Username</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="mail-username"  value="{{ ConfigHelper::getValueByKey('mail-username') }}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Mail Password</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="mail-password"  value="{{ ConfigHelper::getValueByKey('mail-password') }}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Mail Encription</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="mail-encryption"  value="{{ ConfigHelper::getValueByKey('mail-encryption') }}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Mail Address</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="mail-address"  value="{{ ConfigHelper::getValueByKey('mail-address') }}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Mail Name</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="mail-name"  value="{{ ConfigHelper::getValueByKey('mail-name') }}" class="form-control">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary" data-toggle='tooltip' data-placement='top' data-title='Simpan perubahan pada form.'>
                                        <i class="fas fa-save"></i>&nbsp;Simpan Perubahan
                                    </button>
                                    <button type="reset" class="btn btn-danger" data-toggle='tooltip' data-placement='top' data-title='Reset isian form.'>
                                        <i class="fas fa-sync"></i>&nbsp;Reset Form
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection