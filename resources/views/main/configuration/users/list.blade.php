@extends('layouts.app')
@section('title') {{ $title }} @endsection
@section('script')
    <script src="{{ asset($script) }}"></script>
@endsection
@section('content')

 <!-- Content Header (Page header) -->
 <section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ $title }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i>&nbsp;Halaman Utama</a></li>
                    <li class="breadcrumb-item"><a href="#">Pengaturan</a></li>
                    <li class="breadcrumb-item active">{{ $title }}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-table"></i>&nbsp;Daftar {{ $title }}
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            @can('add_'.$route)
                                <div class="clearfix">
                                    <div class="float-right">
                                        <a href="{{ route($route.".create") }}" class="btn btn-success" data-toggle='tooltip' data-placement='top' data-title='Tambah data baru'>
                                            <i class="fas fa-plus"></i>&nbsp;Tambah Data
                                        </a>
                                    </div>
                                </div>
                                <p></p>
                                <p></p>
                            @endcan
                            <table id="table-data" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th width="20" class="text-center">No</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Telepon</th>
                                        <th>Hak Akses</th>
                                        <th class="text-center" width="150">Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection