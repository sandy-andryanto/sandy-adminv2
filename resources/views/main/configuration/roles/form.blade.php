@extends('layouts.app')
@section('title') {{ $title }} @endsection
@section('script')
    <script src="{{ asset($script) }}"></script>
@endsection
@section('content')

 <!-- Content Header (Page header) -->
 <section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ $title }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i>&nbsp;Halaman Utama</a></li>
                    <li class="breadcrumb-item"><a href="#">Referensi</a></li>
                    <li class="breadcrumb-item"><a href="{{ route($route.".index") }}">{{ $title }}</a></li>
                    <li class="breadcrumb-item active">{{ is_null($model->id) ? "Tambah" : "Edit" }}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-edit"></i>&nbsp;Form {{ $title }}
                        </h3>
                    </div>
                    <form method="POST" id="form-submit" class="form-horizontal" action="{{ is_null($model->id) ? route($route.".store") : route($route.".update", array("id"=> $model->id)) }}" autocomplete="off">
                        {{ csrf_field() }}
                        {{ !is_null($model->id) ? method_field('PATCH') : null  }}
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama <span class="text-danger">*</span></label>
                                <div class="col-sm-10">
                                    <input type="text" id="name" name="name" value="{{ $model->name ? $model->name : old('name') }}" required="required" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}">
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Deskripsi </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="description" id="description" rows="4">{{ $model->description ? $model->description : old('description') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Detail Hak Akses</label>
                                <div class="col-sm-10">
                                    @if (!is_null($model->id))
                                        <input type="hidden" class="is_edit" value="1" />
                                        <input type="hidden" id="permission_data" value='{!! json_encode($permission_data) !!}' />
                                        <input type="hidden" id="route_id" value='{{ $model->id }}' />
                                    @endif
                                    <table class="table table-bordered" id="table-create-edit">
                                        <tr class="text-center bg-secondary">
                                            <td rowspan="3" class="text-center"><strong>Modul & Fitur Aplikasi</strong></td>
                                            <td colspan="4" class="text-center"><strong><input type="checkbox"  id="checked-all" />&nbsp;Pilih Semua</strong></td>
                                        </tr>
                                        <tr class="text-center bg-secondary">
                                            <td><strong><i class="fa fa-search"></i>&nbsp;View</strong></td>
                                            <td><strong><i class="fa fa-plus"></i>&nbsp;Tambah</strong></td>
                                            <td><strong><i class="fa fa-edit"></i>&nbsp;Edit</strong></td>
                                            <td><strong><i class="fa fa-trash"></i>&nbsp;Hapus</strong></td>
                                        </tr>
                                        <tr class="text-center bg-secondary">
                                            <td><input type="checkbox" class=" view checked-header" id="checked-view"></td>
                                            <td><input type="checkbox" class=" create checked-header" id="checked-create"></td>
                                            <td><input type="checkbox" class=" edit checked-header" id="checked-edit"></td>
                                            <td><input type="checkbox" class=" delete checked-header" id="checked-delete"></td>
                                        </tr>
                                        {!! UserHelper::generateTablePermission() !!}
                                    </table>
                                    @if ($errors->has('routes'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Modul / Fitur aplikasi silahkan isi</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <a href="{{ route($route.".index") }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Kembali ke daftar">
                                                <i class="fas fa-arrow-left"></i>&nbsp;Kembali
                                            </a>
                                        </div>
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-primary" data-toggle='tooltip' data-placement='top' data-title='Simpan perubahan pada form.'>
                                                <i class="fas fa-save"></i>&nbsp;Simpan Perubahan
                                            </button>
                                            <button type="reset" class="btn btn-danger" data-toggle='tooltip' data-placement='top' data-title='Reset isian form.'>
                                                <i class="fas fa-sync"></i>&nbsp;Reset Form
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection