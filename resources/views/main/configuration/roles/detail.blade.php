@extends('layouts.app')
@section('title') {{ $title }} @endsection
@section('script')
    <script src="{{ asset($script) }}"></script>
@endsection
@section('content')

 <!-- Content Header (Page header) -->
 <section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ $title }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"><i class="fas fa-home"></i>&nbsp;Halaman Utama</a></li>
                    <li class="breadcrumb-item"><a href="#">Pengaturan</a></li>
                    <li class="breadcrumb-item"><a href="{{ route($route.".index") }}">{{ $title }}</a></li>
                    <li class="breadcrumb-item active">Detail</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <div class="container-fluid">
        @include('layouts.alert')
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-search"></i>&nbsp;Detail {{ $title }}
                        </h3>
                    </div>
                    <div class="card-body">
                        <a href="{{ route($route.".index") }}" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Kembali ke daftar">
                            <i class="fas fa-arrow-left"></i>&nbsp;Kembali
                        </a>
                        @can('add_'.$route)
                            <a href="{{ route($route.".create") }}" class="btn btn-success" data-toggle='tooltip' data-placement='top' data-title='Tambah data baru'>
                                <i class="fa fa-plus"></i>&nbsp;Tambah Data
                            </a>
                        @endcan
                        @can('edit_'.$route)
                        <a href="{{ route($route.".edit", array('id'=> $model->id)) }}" class="btn btn-warning" data-toggle='tooltip' data-placement='top' data-title='Edit Data'>
                            <i class="fa fa-edit"></i>&nbsp;Edit Data
                        </a>
                        @endcan
                        @can('delete_'.$route)
                        <a href="{{ route($route.".destroy", array('id'=> $model->id)) }}" id="btn-delete" data-redirect="{{ route($route.".index") }}" data-id="{{ $model->id }}" class="btn btn-danger" data-toggle='tooltip' data-placement='top' data-title='Delete Data'>
                            <i class="fa fa-trash"></i>&nbsp;Hapus Data
                        </a>
                        @endcan
                        <h1></h1>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <td width="100">Nama</td>
                                        <td width="10">:</td>
                                        <td>{{ $model->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Deskripsi</td>
                                        <td>:</td>
                                        <td>{{ $model->description }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <h1></h1>
                        <input type="hidden" id="permission_data" value='{!! json_encode($permission_data) !!}' />
                        <input type="hidden" id="route_id" value='{{ $model->id }}' />
                        <table class="table table-bordered d-none" id="table-show">
                            <tr class="text-center bg-secondary">
                                <td rowspan="3" class="text-center"><strong>Modul & Fitur Aplikasi</strong></td>
                            </tr>
                            <tr class="text-center bg-secondary">
                                <td><strong><i class="fa fa-search"></i>&nbsp;View</strong></td>
                                <td><strong><i class="fa fa-plus"></i>&nbsp;Tambah</strong></td>
                                <td><strong><i class="fa fa-edit"></i>&nbsp;Edit</strong></td>
                                <td><strong><i class="fa fa-trash"></i>&nbsp;Hapus</strong></td>
                            </tr>
                            <tr class="text-center bg-secondary">
                                <td><input type="checkbox" class=" view checked-header" id="checked-view"></td>
                                <td><input type="checkbox" class=" create checked-header" id="checked-create"></td>
                                <td><input type="checkbox" class=" edit checked-header" id="checked-edit"></td>
                                <td><input type="checkbox" class=" delete checked-header" id="checked-delete"></td>
                            </tr>
                            {!! UserHelper::generateTablePermission() !!}
                        </table>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

@endsection