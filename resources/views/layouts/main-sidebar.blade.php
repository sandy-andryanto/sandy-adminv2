<!-- Brand Logo -->
<a href="{{ route('home') }}" class="brand-link">
    <img src="{{ asset('app/img/favicon.png') }}" alt="{{ env('APP_NAME', 'Laravel') }}"  class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">{{ env('APP_NAME', 'Laravel') }}</span>
</a>

<!-- Sidebar -->
<div class="sidebar d-none">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{{ UserHelper::getRealPhoto() }}" class="img-circle elevation-2 img-user" alt="User Image">
        </div>
        <div class="info">
            <a href="#" class="d-block">{{ UserHelper::getRealName() }}</a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
            {!! UserHelper::getAdminMenu() !!}
            <li class="nav-item">
                <a href="{{ route('notifications.index') }}" class="nav-link {{ Route::currentRouteName() == 'notifications.index' || Route::currentRouteName() == 'notifications.show' ? 'active' : '' }}">
                  <i class="nav-icon far fa-bell"></i>
                  <p>
                    Pemberitahuan
                    <span class="badge badge-warning right notification-count">0</span>
                  </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('profile.index') }}" class="nav-link {{ Route::currentRouteName() == 'profile.index' ? 'active' : '' }}">
                    <i class="nav-icon fas fa-user-plus"></i>
                    <p>Profil Saya</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('password.index') }}" class="nav-link {{ Route::currentRouteName() == 'password.index' ? 'active' : '' }}">
                    <i class="nav-icon fas fa-lock"></i>
                    <p>Ubah Password</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link">
                    <i class="nav-icon fas fa-sign-out-alt"></i>
                    <p>Keluar</p>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->