<div class="float-right d-none d-sm-block">
    <a href="{{ route('home') }}">
        <b>Versi</b>&nbsp;&nbsp;{{ env('APP_VERSION', '1.0') }}
    </a>
</div>
<strong class="text-info">Copyright © {{ date('Y') }} {{ env('APP_NAME') }}</strong>.