<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ env('APP_NAME', 'Laravel') }} | @yield('title')</title>
    <meta name="api-token" content={!! json_encode(Auth::guard('api')->tokenById(Auth::User()->id)) !!}>
    <meta name="base-url" content="{{ url('') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="elfinder-url" content="{{ route('galleries.editor') }}">
    <link rel="icon" type="image/png" href="{{ asset('app/img/favicon.png') }}" />
    @include('layouts.stylesheet')
    @yield('stylesheet')
    <link rel="stylesheet" href="{{ asset('app/css/app.core.css?'.time()) }}">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            @include('layouts.main-header')
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar main-sidebar sidebar-dark-secondary elevation-4">
            @include('layouts.main-sidebar')
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            @include('layouts.main-footer')
        </footer>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->
    @include('layouts.script')
    @yield('script')
</body>

</html>