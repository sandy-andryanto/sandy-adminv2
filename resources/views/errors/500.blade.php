@extends('layouts.error')
@section('title') 500 @endsection
@section('content')
    <h3><i class="fas fa-exclamation-triangle text-warning"></i> 500 - Ada kesalahan teknis pada website</h3>
    <p>
        Maaf, Silahkan refresh kembali halaman ini, atau kontak administrator.
    </p>
    <a href="{{ url()->previous() }}" class="btn btn-secondary" data-toggle='tooltip' data-placement='top' data-title='Kembali'>
        <i class="fas fa-arrow-left"></i>&nbsp;Kembali
    </a>
@endsection