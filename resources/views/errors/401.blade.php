@extends('layouts.error')
@section('title') 401 @endsection
@section('content')
    <h3><i class="fas fa-exclamation-triangle text-warning"></i> 401 -  Akses Ditolak!</h3>
    <p>
        Maaf, Permintaan anda telah ditolak pada website kami.
    </p>
    <a href="{{ url()->previous() }}" class="btn btn-secondary" data-toggle='tooltip' data-placement='top' data-title='Kembali'>
        <i class="fas fa-arrow-left"></i>&nbsp;Kembali
    </a>
@endsection