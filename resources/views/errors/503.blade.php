@extends('layouts.error')
@section('title') 503 @endsection
@section('content')
    <h3><i class="fas fa-exclamation-triangle text-warning"></i> 503 - Halaman ini sedang diperbaiki</h3>
    <p>
        Maaf, Mohon tunggu beberapa waktu untuk mengakses halaman ini.
    </p>
    <a href="{{ url()->previous() }}" class="btn btn-secondary" data-toggle='tooltip' data-placement='top' data-title='Kembali'>
        <i class="fas fa-arrow-left"></i>&nbsp;Kembali
    </a>
@endsection