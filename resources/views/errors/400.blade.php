@extends('layouts.error')
@section('title') 400 @endsection
@section('content')
    <h3><i class="fas fa-exclamation-triangle text-warning"></i> 400 - Permintaan Gagal</h3>
    <p>
        Maaf, Browser anda tidak bisa menerima permintaan pada website ini.
    </p>
    <a href="{{ url()->previous() }}" class="btn btn-secondary" data-toggle='tooltip' data-placement='top' data-title='Kembali'>
        <i class="fas fa-arrow-left"></i>&nbsp;Kembali
    </a>
@endsection