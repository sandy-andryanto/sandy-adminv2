@extends('layouts.error')
@section('title') 403 @endsection
@section('content')
    <h3><i class="fas fa-exclamation-triangle text-warning"></i> 403 - Akses Ditolak</h3>
    <p>
        Maaf, Anda tidak diperkenankan mengakses halaman ini oleh administrator.
    </p>
    <a href="{{ url()->previous() }}" class="btn btn-secondary" data-toggle='tooltip' data-placement='top' data-title='Kembali'>
        <i class="fas fa-arrow-left"></i>&nbsp;Kembali
    </a>
@endsection