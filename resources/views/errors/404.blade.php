@extends('layouts.error')
@section('title') 404 @endsection
@section('content')
    <h3><i class="fas fa-exclamation-triangle text-warning"></i> 404 - Halaman Tidak Ditemukan</h3>
    <p>
        Maaf, halaman yang anda akses tidak ditemukan, Halaman yang anda akses kemungkinan telah dihapus.
    </p>
    <a href="{{ url()->previous() }}" class="btn btn-secondary" data-toggle='tooltip' data-placement='top' data-title='Kembali'>
        <i class="fas fa-arrow-left"></i>&nbsp;Kembali
    </a>
@endsection