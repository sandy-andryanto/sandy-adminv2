@extends('layouts.auth')
@section('title') Pemulihan Akun @endsection
@section('content')


<div class="text-center">
    <img src="{{ asset('app/img/favicon.png') }}" class="img img-thumbnail img-responssive" width="100">
    <p></p>
</div>

<p class="login-box-msg h2">
    <strong class="text-uppercase">{{ env('APP_NAME', 'Laravel') }}</strong>
</p>

@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif

<p class="text-justify"> Masukkan email yang biasa Anda gunakan untuk masuk ke <strong>{{ env('APP_NAME', 'Laravel') }}</strong> dan sistem akam mengirim tautan untuk mereset password.</p>
<form action="{{ route('password.email') }}" method="post" autocomplete="off">
    {{ csrf_field() }}
    <div class="input-group mb-3">
        <input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" required="required" placeholder="Email Aktif" value="{{ old('email') }}">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="fas fa-envelope"></span>
            </div>
        </div>
        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <div class="input-group mb-3">
        <input type="text" name="captcha" class="form-control {{ $errors->has('captcha') ? ' is-invalid' : '' }}" required="required" placeholder="Kode Captcha">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="fas fa-qrcode"></span>
            </div>
        </div>
        @if ($errors->has('captcha'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('captcha') }}</strong>
            </span>
        @endif
    </div>

    <div id="captcha-section" class="text-center">
        <span id="captcha-img">
            {!! captcha_img('flat') !!}
        </span>
        <a href="javascript:void(0);" id="btn-reload-captcha" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Ganti Kode Captcha">
            <i class="fas fa-redo-alt"></i>
        </a>
    </div>
    <div class="clearfix"></div>
    <p></p>
    <p></p>
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-secondary btn-block" data-toggle='tooltip' data-placement='top' data-title='Kirim Permintaan'>
                <i class="fas fa-paper-plane"></i>&nbsp;Kirim Permintaan
            </button>
        </div>
        <!-- /.col -->
    </div>
</form>
<p></p>
<p class="mb-0">
    <a href="{{ route('login') }}" class="text-center" data-toggle='tooltip' data-placement='top' data-title='Klik disini jika sudah belum memiliki akun.'>Sudah memiliki akun ? Masuk disini.</a>
</p>

@endsection