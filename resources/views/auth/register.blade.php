@extends('layouts.auth')
@section('title') Daftarkan Akun @endsection
@section('content')

<div class="text-center">
    <img src="{{ asset('app/img/favicon.png') }}" class="img img-thumbnail img-responssive" width="100">
    <p></p>
</div>

<p class="login-box-msg h2">
    <strong class="text-uppercase">{{ env('APP_NAME', 'Laravel') }}</strong>
</p>



<form action="{{ route('register') }}" method="post" autocomplete="off">
    {{ csrf_field() }}
    <div class="input-group mb-3">
        <input type="text" name="username" class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}" required="required" placeholder="Username" value="{{ old('username') }}">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="fas fa-user"></span>
            </div>
        </div>
        @if ($errors->has('username'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
        @endif
    </div>
    <div class="input-group mb-3">
        <input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" required="required" placeholder="Email" value="{{ old('email') }}">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="fas fa-envelope"></span>
            </div>
        </div>
        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <div class="input-group mb-3">
        <input type="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" required="required" placeholder="Kata Sandi">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="fas fa-lock"></span>
            </div>
        </div>
        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
    <div class="input-group mb-3">
        <input type="password" name="password_confirmation" class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" required="required" placeholder="Konfirmasi Kata Sandi">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="fas fa-lock"></span>
            </div>
        </div>
        @if ($errors->has('password_confirmation'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
    </div>
    <div class="input-group mb-3">
        <input type="text" name="captcha" class="form-control {{ $errors->has('captcha') ? ' is-invalid' : '' }}" required="required" placeholder="Kode Captcha">
        <div class="input-group-append">
            <div class="input-group-text">
                <span class="fas fa-qrcode"></span>
            </div>
        </div>
        @if ($errors->has('captcha'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('captcha') }}</strong>
            </span>
        @endif
    </div>

    <div id="captcha-section" class="text-center">
        <span id="captcha-img">
            {!! captcha_img('flat') !!}
        </span>
        <a href="javascript:void(0);" id="btn-reload-captcha" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Ganti Kode Captcha">
            <i class="fas fa-redo-alt"></i>
        </a>
    </div>
    <div class="clearfix"></div>
    <p></p>
    <p></p>

    <div class="row">
        <!-- /.col -->
        <div class="col-12">
            <button type="submit" class="btn btn-secondary btn-block" data-toggle='tooltip' data-placement='top' data-title='Daftarkan Akun Baru'>
                <i class="fas fa-edit"></i>&nbsp;Daftarkan Akun
            </button>
        </div>
        <!-- /.col -->
    </div>
</form>
<p></p>
<p class="mb-0">
    <a href="{{ route('login') }}" class="text-center" data-toggle='tooltip' data-placement='top' data-title='Klik disini jika sudah belum memiliki akun.'>Sudah memiliki akun ? Daftar disini.</a>
</p>

@endsection