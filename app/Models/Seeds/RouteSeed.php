<?php 

namespace App\Models\Seeds;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Entities\Auth\Role;
use App\Models\Entities\Auth\Route;
use App\Models\Entities\Auth\Permission;
use App\Helpers\CommonHelper;

class RouteSeed extends Model{

    public static function init(){
        $app = new RouteSeed;
        $app->run();
    }

    private function syncRouteResource($role, array $routes){
        $route_id = array();
        $routes = Route::whereIn("id", $routes)->get();
        foreach($routes as $route){
            $route_id[] = $route->id;
            if(!is_null($route->parent_id)){
                $route_id[] = $route->parent_id;
            }
        }
        $role->Routes()->sync($route_id);
    }

    private function syncPermissions(){
        $route_ids = array();
        $routes = Route::all();
        foreach($routes as $r){
            $route_ids[] = $r->id;
            if(!is_null($r->route)){
                $route_name = trim($r->route);
                $route_name = str_replace(".index", "", $route_name);
                $action = ["view", "add", "edit", "delete"];
                foreach($action as $row){
                    $term = $row."_".$route_name;
                    $permission = Permission::where("name", trim($term))->first();
                    if(is_null($permission)){
                        Permission::firstOrCreate(['name' => $term]);
                    }
                }
            }
        }
        $admin = Role::where("name", trim("ADMIN"))->first();
        if(!is_null($admin)){
            $admin->syncPermissions(Permission::all());
            $this->syncRouteResource($admin, $route_ids);
        }
    }

    private function run(){
        
        $reset = Route::where("id", "<>", 0)->delete();
        $json = file_get_contents(storage_path("seeds/json/app.routes.json"));
        $routes = json_decode($json, true);
		$i = 1;
        foreach ($routes as $row) {
            $this->createRoutes($row, $i);
            $i++;
        }    

        $routes_id = Route::where("id", "<>", 0)->pluck("id")->toArray();
        if(count($routes_id) > 0){
            $roles = Role::where("id", "<>", 0)->get();
            if(count($roles) > 0){
                foreach($roles as $role){
                    $role->routes()->sync($routes_id);
                }
            }
        }

        $this->syncPermissions();

        $client = Role::where("name", trim("CLIENT"))->first();
        $route_deleted = array();
        if(!is_null($client)){
            $ignore_route = array("settings", "users", "roles", "logs", "audits");
            $access_client = array();
            $routes = Route::all();
            foreach($routes as $route){
                $target = CommonHelper::slugify($route->name);
                $ignores = array(
                    'Pengaturan',
                    'Aplikasi',
                    'Pengguna',
                    'Hak Akses',
                    'Log Data',
                    'Audit Trail'  
                );
                foreach($ignores as $ignore){
                    $dest = CommonHelper::slugify($ignore);
                    if($dest == $target){
                        $route_deleted[] = $route->id;
                    }else{
                        $route_name = trim($route->route);
                        $route_name = str_replace(".index", "", $route_name);
                        if(strlen($route_name) > 0 && !in_array($route_name, $ignore_route)){
                            $access_client[] = $route_name;
                        }
                       
                    }
                }
            }

            if(count($route_deleted) > 0){
                DB::table("auth_routes_roles")->where("role_id", $client->id)->whereIn("route_id", $route_deleted)->delete();
            }

            if(count($access_client) > 0){
                $allow_client = array();
                $access_client = array_unique($access_client);
                $access_client = array_values($access_client);
                $actions = ["view", "add", "edit", "delete"];
                foreach($actions as $action){
                    foreach($access_client as $row){
                        $term = $action."_".$row;
                        $allow_client[] = trim($term);
                    }
                }

                $permission_to_client = Permission::whereIn("name", $allow_client)->get();
                $client->syncPermissions($permission_to_client);
            }

        }   

    }

    private function createRoutes($row, $sort, $parent_id = null){
        $insertData = array(
            "parent_id" => $parent_id,
            "name" => isset($row["label"]) ? $row["label"] : null,
            "route" => isset($row["route"]) ? $row["route"] : null,
            "icon" => isset($row["icon"]) ? $row["icon"] : null,
            "sort" => $sort,
        );
        $navigation = Route::create($insertData);
        $route_id = $navigation->id;
        if (isset($row["childs"])) {
            $childs = $row["childs"];
            if (count($childs) > 0) {
                $i = 1;
                foreach ($childs as $child) {
                    $this->createRoutes($child, $i, $route_id);
                    $i++;
                }
            }
        }
    }

}