<?php 

namespace App\Models\Seeds;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\CommonHelper;
use Faker\Factory as Faker;

class RefrenceSeed extends Model{

    public static function init(){

        try {

            $app = new self;
            $app->run();
          
        } catch (\Exception $e) {
            
        }
    }

    public function run(){

        // Kontak
        for($i = 1; $i <=3; $i++){
            $faker = Faker::create();
            $formData = array(
                'name'=> $faker->city,
                'email'=> $faker->email,
                'phone'=> $faker->phoneNumber,
                'website'=> $faker->freeEmailDomain,
                'address'=> $faker->streetAddress,
                "created_at"=> date('Y-m-d H:i:s'),
                "updated_at"=> date('Y-m-d H:i:s')
            );
            DB::table("ref_contacts")->insert($formData);
        }

    
    }

}