<?php 

namespace App\Models\Core;

use Illuminate\Support\Facades\Route as RouteApp;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Entities\Auth\User;

class AdminMenu extends Model{

    private $menu = null;
	private $route_path;

    public static function getMenu($user_id){
        $model = new self;
        return $model->generateMenu($user_id);
    }

    public function generateMenu($user_id){

        $role_id = DB::table("auth_model_has_roles")->where("model_type", User::class)->where("model_id", $user_id)->pluck("role_id")->toArray();
        $route_id = DB::table("auth_routes_roles")->whereIn("role_id", $role_id)->pluck("route_id")->toArray();
        $parents = DB::table("auth_routes")->whereIn("id", $route_id)->where("parent_id", null)->orderBy("sort", "ASC")->get();
        
        if ($parents->isNotEmpty()) { 
            foreach($parents as $route){
                $this->createMenu($route, $role_id);
            }
        }

        return $this->menu;
    }

    public function createMenu($parent, array $role_id){

        $url = strlen($parent->route) == 0 ? "javascript:void(0);" : $this->getRouteApp($parent->route);
        $icon = is_null($parent->icon) ? "fas fa-angle-right" : "fa " . $parent->icon;

        $current_route = RouteApp::currentRouteName();
        $current_route = str_replace("create", "index", $current_route);
        $current_route = str_replace("edit", "index", $current_route);
        $current_route = str_replace("show", "index", $current_route);

        $active = $current_route == $parent->route ? 'active' : '';
        $route_id = DB::table("auth_routes_roles")->whereIn("role_id", $role_id)->pluck("route_id")->toArray();
        $data = DB::table("auth_routes")->where("parent_id", $parent->id)->orderBy("name", "ASC")->get();
        $target = "_self";

        if ($data->isNotEmpty()) { 

            $this->menu .= ' 
                <li class="nav-item" data-id="'.$parent->id.'" data-parent="0">
                    <a href="'.$url.'" target="'.$target.'" class="nav-link">
                        <i class="nav-icon '.$icon.'"></i>
                        <p>
                            '.$parent->name.'
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                ';
				foreach ($data as $row) {
					$this->createMenu($row, $role_id);
				}		
			$this->menu .= '
					</ul>
				</li>
			';
            
        }else{

            $getRoute = DB::table("auth_routes")->where("id", $parent->id)->first();
            $icon = "fas fa-angle-right";
			if(!is_null($getRoute->icon)){
				$icon = "fas ".$getRoute->icon;
			}

            $this->menu .= '
                <li class="nav-item">
                    <a href="'.$url.'" class="nav-link '.$active.'" data-id="'.$parent->id.'" data-parent="'.$getRoute->parent_id.'">
                    <i class="'.$icon.' nav-icon"></i>
                        <p>'.$parent->name.'</p>
                    </a>
                </li>
            ';

        }

    }

    public function getRouteApp($route){
        return RouteApp::has($route) ? route($route) : 'javascript:void(0);';
    }

    public function can_view($route_id, $role_id){
        $getRoute = DB::table("auth_routes")->where("id", $route_id)->first();
        $route = trim(str_replace(".index", "", $getRoute->route));
        $permission = "view_".$route;
        $permission_id = DB::table("auth_permissions")->where("name", trim($permission))->pluck("id")->toArray();
        $check = DB::table("auth_role_has_permissions")->whereIn("permission_id", $permission_id)->whereIn("role_id", $role_id)->count();
        return $check == 0 ? false : true;
    }

}