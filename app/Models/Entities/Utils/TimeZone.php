<?php

namespace App\Models\Entities\Utils;

use Illuminate\Database\Eloquent\Model;

class TimeZone extends Model {

    public static function getData(){
        $json = file_get_contents(storage_path("seeds/json/timezones.json"));
        $timezones = json_decode($json, true);
        return $timezones;
    }

}