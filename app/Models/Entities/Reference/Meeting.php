<?php 

namespace App\Models\Entities\Reference;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Meeting extends Model implements Auditable{

    use \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $table = 'ref_meetings';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        "date_meeting",
        "title",
        "city",
        "state",
        "address",
        "body"
    ];
    
}