<?php 

namespace App\Models\Entities\Reference;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Task extends Model implements Auditable{

    use \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $table = 'ref_tasks';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        "date_first",
        "date_last",
        "title",
        "body",
        "priority",
        "status"
    ];
    
}