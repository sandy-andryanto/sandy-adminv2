<?php 

namespace App\Models\Entities\Reference;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Path extends Model implements Auditable{

    use \OwenIt\Auditing\Auditable;

    protected $table = 'ref_paths';

    protected $fillable = [
        'model_id',
        'model_type',
        'path'
    ];
    
    public static function getByModel($id, $model_type){
        $model = self::where("model_id", $id)->where("model_type", $model_type)->first();
        return !is_null($model) ? $model->path : "-";
    }

    public static function syncModel($id, $model_type, $path){
        if(strlen($path) > 0){
            $path_arr = explode(" » ", $path);
            array_pop($path_arr);   
            $path = implode(" » ", $path_arr);
            $model = self::where("model_id", $id)->where("model_type", $model_type)->first();
            if(!is_null($model)){
                $model->model_id = $id;
                $model->model_type = $model_type;
                $model->path = $path;
                $model->save();
                return $model;
            }else{
                $insertData = array(
                    'model_id'=> $id,
                    'model_type'=> $model_type,
                    'path'=> $path
                );
                return self::create($insertData);
            }
        }else{
            return NULL;
        }
    }

}