<?php 

namespace App\Models\Entities\Reference;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Customer extends Model implements Auditable{

    use \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $table = 'ref_customers';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        "name",
        "email",
        "phone",
        "job_title",
        "company_name",
        "website",
        "city",
        "state",
        "zip_code",
        "street_address"
    ];
    
}