<?php 

namespace App\Models\Entities\Reference;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Contact extends Model implements Auditable{

    use \OwenIt\Auditing\Auditable, SoftDeletes;

    protected $table = 'ref_contacts';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'email',
        'phone',
        'website',
        'address'
    ];
    
}