<?php 

namespace App\Models\Entities\Auth;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Entities\Auth\Role;

class Route extends Model{

    protected $table = 'auth_routes';
    protected $fillable = [
        'parent_id',
        'name',
        'icon',
        'route',
        'sort'
    ];

    public function Roles() {
        return $this->belongsToMany(Role::class, "auth_routes_roles");
    }

    public static function getRoleId($user_id){
        return DB::table("auth_model_has_roles")
            ->where("model_type", User::class)
            ->where("model_id", $user_id)
            ->select("role_id")
            ->groupBy("role_id")
            ->pluck("role_id")
            ->toArray();
    }

    public static function getRouteMenu(array $role_id, $parent_id = null){
        $route_id =  DB::table("auth_routes_roles")
            ->whereIn("role_id", $role_id)
            ->select("route_id")
            ->groupBy("route_id")
            ->pluck("route_id")
            ->toArray();
        return self::whereIn("id", $route_id)->where("parent_id", $parent_id)->orderBy("sort", "ASC")->get();
          
    }

    public static function getRouteHome(array $role_id){
        $route_id =  DB::table("auth_routes_roles")
            ->whereIn("role_id", $role_id)
            ->select("route_id")
            ->groupBy("route_id")
            ->pluck("route_id")
            ->toArray();
        return self::whereIn("id", $route_id)->where("route", "!=", null)->orderBy("sort", "ASC")->first();
          
    }
    
}