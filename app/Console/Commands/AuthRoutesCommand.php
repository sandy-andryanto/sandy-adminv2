<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Seeds\RouteSeed;

class AuthRoutesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth:route-permission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new routes with permission';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        RouteSeed::init();
        $this->info('Route aplikasi berhasil diperbaharui!');
    }
}
