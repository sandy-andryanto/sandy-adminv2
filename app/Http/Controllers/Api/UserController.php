<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as LaravelController;
use Illuminate\Http\Request;
use App\Utils\CropImage;
use App\Models\Entities\Auth\User;
use Faker\Factory as Faker;
use App\Models\Entities\Auth\Notification;

/**
 * @Middleware("api")
 * @Middleware("xss")
 * @Middleware("auth:api")
 * @Controller(prefix="api/user")
 */
class UserController extends LaravelController{


    /**
     * 
     * @Post("/notifications/list", as="api.user.notifications")
     */
    public function notification(Request $request){
        $user = \Auth::User();
        $unread = Notification::where("user_id", $user->id)->where("readed_at", null)->count();
        $list = Notification::where("user_id", $user->id)->where("readed_at", null)->orderBy("id", "DESC")->take(5)->get();
        $response = array(
            "unread"=> $unread,
            "list"=> $list
        );
        return response()->json($response);
    }

    /**
     * 
     * @Post("/update/profile/image", as="api.user.update.image")
     */
    public function updateProfileImage(Request $request){

        $avatarSrc = isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null;
        $avatarData = isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null;
        $avatarFile = isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null;
        $crop = new CropImage($avatarSrc, $avatarData, $avatarFile);
    
        $path = $crop->getResult();
        $file_name = basename($path);
        $name = "Foto Profil";
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $size = filesize($path);
        $data = addslashes($path);
        $file_path = null;

        $user = \Auth::User();
        // Hapus sebelumnya

        $file_path_current = $user->image;
        if(strlen($file_path_current) > 0){
            if(file_exists($file_path_current)){
                @unlink($file_path_current);
            }
        }

        $temp = $path;
        $dst = public_path("uploads/".$file_name);
        $copy = @copy($temp, $dst);
        if($copy){
            $file_path = "uploads/".$file_name;
            if(file_exists($crop->getResult())){
                @unlink($crop->getResult());
            }

            if(file_exists($crop->getOriginal())){
                @unlink($crop->getOriginal());    
            }

            // Simpan Data Sekarang
            $user->image =  $file_path;
            $user->save();

        }

        $response = array(
            'state' => 200,
            'message' => $crop->getMsg(),
            'result' => url($file_path)
        );
        return response()->json($response);
    }

    
    
}
