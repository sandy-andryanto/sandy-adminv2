<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\CrudController;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Route as RouteApp;
// Load Models
use App\Models\Entities\Auth\Role;
use App\Models\Entities\Auth\User;

/**
 * @Resource("users")
 * @Middleware("web")
 * @Middleware("auth")
 * @Middleware("xss")
 * @Middleware("timeout")
 * @Controller(prefix="configuration")
 */
class UserController extends CrudController{

    protected $data = array();
    protected $view_source = "main.configuration.users";
    protected $title = "Pengguna";
    protected $route_name = "users";
    protected $model = User::class;
    protected $script = 'app/js/users.js';

    /**
     * 
     * @Post("/users/datatable", as="users.datatable")
     */
    public function datatable(Request $request){

        $user_id = \Auth::User()->id;
        $eloquent = new $this->model;
        $result = $eloquent->where("id", "!=", $user_id)->orderBy("id", "DESC")->get();
        $user = \Auth::User();
        return Datatables::of($result)
            ->addColumn('roles', function($result) {
                return implode(", ", $result->Roles->pluck('name')->toArray());
            })
            ->addColumn('action', function($result) use ($user){

                $route_view = route($this->route_name.".show", array("id"=> $result->id));
                $route_edit = route($this->route_name.".edit", array("id"=> $result->id));
                $route_delete = route($this->route_name.".destroy", array("id"=> $result->id));
                $action = "<a href='".$route_view."' class='btn btn-sm btn-info' data-toggle='tooltip' data-placement='top' data-original-title='Lihat Detail'><i class='fas fa-search'></i></a>";

                if($user->can("edit_".$this->route_name)){
                    $action .= "&nbsp;<a href='".$route_edit."' class='btn btn-sm btn-warning' data-toggle='tooltip' data-placement='top' data-original-title='Edit Data'><i class='fas fa-edit'></i></a>";
                }

                if($user->can("delete_".$this->route_name)){
                    $action .= "&nbsp;<a href='".$route_delete."' class='btn btn-sm btn-danger btn-delete' data-id='".$result->id."' data-toggle='tooltip' data-placement='top' data-original-title='Hapus Data'><i class='fas fa-trash'></i></a>";
                }

                return $action;
            })->make(true);
    }

    public function create(){
        $entity = new $this->model;
        $entity->is_admin = 0;
        $entity->verified = 0;
        $entity->is_banned = 0;
        $this->data["model"] = $entity;
        $this->data["title"] = $this->title;
        $this->data["route"] = $this->route_name;
        $this->data["script"] = asset($this->script."?".time());
        $this->data["roles"] = Role::all()->pluck('name', 'id')->toArray();
        $this->data["role_selected"] = array();
        return view($this->view_source.'.form', $this->data);
    }

    public function edit($id){

        if(is_null($id)){
            return abort(404);
        }

        $eloquent = new $this->model;
        $entitiy = $eloquent->where("id", $id)->first();

        if(is_null($entitiy)){
            return abort(404);
        }

        $this->data["model"] = $entitiy;
        $this->data["title"] = $this->title;
        $this->data["route"] = $this->route_name;
        $this->data["script"] = asset($this->script."?".time());
        $this->data["roles"] = Role::all()->pluck('name', 'id')->toArray();
        $this->data["role_selected"] = $entitiy->Roles->pluck('id')->toArray();
        return view($this->view_source.'.form', $this->data);
    }

    public function show($id){

        if(is_null($id)){
            return abort(404);
        }

        $eloquent = new $this->model;
        $entitiy = $eloquent->where("id", $id)->first();

        if(is_null($entitiy)){
            return abort(404);
        }


        $this->data["model"] = $entitiy;
        $this->data["title"] = $this->title;
        $this->data["route"] = $this->route_name;
        $this->data["script"] = asset($this->script."?".time());
        return view($this->view_source.'.detail', $this->data);

    }

    public function store(Request $request){

        $rules = array(
            'username' => 'required|alpha_dash|unique:auth_users',
            'email' => 'required|string|email|max:255|unique:auth_users',
            'password' => 'required|string|min:6',
            'roles' => 'required|min:1',
        );

        if($request->get("phone")){
            $rules["phone"] = 'required|regex:/^[0-9]+$/|unique:auth_users';
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }else{

            $groups = Role::whereIn("id", $request->get("roles"))->get()->pluck("name")->toArray();
            $user = new User;
            $user->username = $request->get("username");
            $user->email = $request->get("email");

            if($request->get("phone")){
                $user->phone = $request->get("phone");
            }

            $user->name = $request->get("name");
            $user->gender = $request->get("gender") ? $request->get("gender") : 0;
            $user->city = $request->get("city");
            $user->address = $request->get("address");

            $user->remember_token = base64_encode($request->get("email"));
            $user->verified = 1;
            $user->verification_token = base64_encode($request->get("email"));
            $user->password = bcrypt($request->get("password"));
            $user->save();
            
            
            $user->syncRoles($groups);

            $id = $user->id;
 
            return redirect()->route("users.show", ["id"=> $id])->with('success', "Data ".$this->title." berhasil disimpan!.");
           
        }
    }

    public function update($id, Request $request){
        
        $rules = array(
            'username' => 'required|alpha_dash|unique:auth_users,username,' . $id,
            'email' => 'required|email|unique:auth_users,email,' . $id,
            'roles' => 'required|min:1',
        );

        if($request->get("phone")){
            $rules["phone"] = 'required|regex:/^[0-9]+$/|unique:auth_users,phone,'.$id;
        }

        if($request->get("password")){
            $rules['password'] = "required|string|min:6";
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }else{
            
            $groups = Role::whereIn("id", $request->get("roles"))->get()->pluck("name")->toArray();
            $user = User::where("id", $id)->first();

            $user->username = $request->get("username");
            $user->email = $request->get("email");

            $user->name = $request->get("name");
            $user->gender = $request->get("gender") ? $request->get("gender") : 0;
            $user->city = $request->get("city");
            $user->address = $request->get("address");

            if($request->get("phone")){
                $user->phone = $request->get("phone");
            }

            if($request->get("password")){
                $user->password = bcrypt($request->get("password"));
            }

            $user->save();
            $user->syncRoles($groups);

            return redirect()->route("users.show", ["id"=> $id])->with('success', "Data ".$this->title." berhasil diupdate!.");
        }
    }

    
    
}