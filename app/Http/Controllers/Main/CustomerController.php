<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\CrudController;
use Illuminate\Http\Request;
// Load Models
use App\Models\Entities\Reference\Customer;

/**
 * @Resource("customers")
 * @Middleware("web")
 * @Middleware("auth")
 * @Middleware("xss")
 * @Middleware("timeout")
 * @Controller(prefix="reference")
 */
class CustomerController extends CrudController{

    protected $data = array();
    protected $view_source = "main.reference.customers";
    protected $title = "Pelanggan";
    protected $route_name = "customers";
    protected $model = Customer::class;
    protected $script = 'app/js/customers.js';

    /**
     * 
     * @Post("/customers/datatable", as="customers.datatable")
     */
    public function datatable(Request $request){
        return parent::datatable($request);
    }

    protected function create_validation(){
        return array(
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'street_address' => 'required|string',
        );
    }

    protected function edit_validation($id){
        return array(
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'street_address' => 'required|string',
        );
    }
    
}