<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\CrudController;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

/**
 * @Resource("galleries")
 * @Middleware("web")
 * @Middleware("auth")
 * @Middleware("xss")
 * @Middleware("timeout")
 */
class GalleryController extends CrudController{

    protected $data = array();
    protected $view_source = "main.galleries";
    protected $title = "Media";
    protected $route_name = "galleries";
    protected $model;
    protected $script = 'app/js/galleries.js';

    public function create(){
        return abort(404);
    }

    public function store(Request $request){
        return abort(404);
    }

    public function update($id, Request $request){
        return abort(404);
    }

    public function show($id){
        return abort(404);
    }

    public function edit($id){
        return abort(404);
    }

    public function destroy($id){
        return abort(404);
    }

    /**
     * 
     * @Get("/galleries/editor", as="galleries.editor")
     */
    public function editor(){
        return view("main.galleries.editor");
    }
    
}