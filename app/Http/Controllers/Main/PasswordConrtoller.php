<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller as LaravelController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Entities\Auth\User;

/**
 * @Middleware("web")
 * @Middleware("xss")
 * @Middleware("auth")
 * @Middleware("timeout")
 * @Controller(prefix="account")
 */
class PasswordConrtoller extends LaravelController{

    private $data = array();

    /**
     * 
     * @Get("/password", as="password.index")
     */
    public function index(){
       return view('main.account.password');
    }

    /**
     * 
     * @Post("/password/update", as="password.update")
     */
    public function update(Request $request){
        $rules = array(
            'old_password' => 'required|string',
            'password' => 'required|string|min:6|confirmed',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }else{
            $old_password = $request->get("old_password");
            $password = $request->get("password");
            $user = \Auth::User();
            if (!Hash::check($old_password, $user->password)) { 
                return back()->withErrors(['old_password' => ['Password lama tidak cocok.']]);
            }else{
                $user->fill(['password' => Hash::make($password)])->save();
                \Auth::logout();
                return redirect()->route("login")->with('success', "Password anda berhasil dirubah. Silahkan login kembali!");
            }
        }
    }

    
    
}
