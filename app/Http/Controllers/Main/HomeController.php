<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller as LaravelController;
use Illuminate\Support\Facades\Route as RouteApp;
use Illuminate\Http\Request;

/**
 * @Middleware("web")
 * @Middleware("xss")
 * @Middleware("auth")
 * @Middleware("timeout")
 */
class HomeController extends LaravelController{

   /**
     * 
     * @Get("/home", as="home")
     */
    public function home(){

      $homepage = \Auth::User()->getHomeRoute();
      if(!is_null($homepage)){
         $route = $homepage->route;
         if(RouteApp::has($route)){
           return redirect()->route($route);
         }
      }

      return redirect()->route("profile.index");
   }

    /**
     * 
     * @Get("/", as="welcome")
     */
    public function index(){

       $homepage = \Auth::User()->getHomeRoute();
       if(!is_null($homepage)){
          $route = $homepage->route;
          if(RouteApp::has($route)){
            return redirect()->route($route);
          }
       }

       return redirect()->route("profile.index");
    }

}
