<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\CrudController;
use Illuminate\Http\Request;
// Load Models
use App\Models\Entities\Reference\Task;

/**
 * @Resource("tasks")
 * @Middleware("web")
 * @Middleware("auth")
 * @Middleware("xss")
 * @Middleware("timeout")
 * @Controller(prefix="reference")
 */
class TaskController extends CrudController{

    protected $data = array();
    protected $view_source = "main.reference.tasks";
    protected $title = "Pekerjaan";
    protected $route_name = "tasks";
    protected $model = Task::class;
    protected $script = 'app/js/tasks.js';

    /**
     * 
     * @Post("/tasks/datatable", as="tasks.datatable")
     */
    public function datatable(Request $request){
        return parent::datatable($request);
    }

    protected function create_validation(){
        return array(
            'date_first' => 'required',
            'date_last' => 'required',
            'title' => 'required',
            'body' => 'required',
            'priority' => 'required',
            'status' => 'required',
        );
    }

    protected function edit_validation($id){
        return array(
            'date_first' => 'required',
            'date_last' => 'required',
            'title' => 'required',
            'body' => 'required',
            'priority' => 'required',
            'status' => 'required',
        );
    }
    
}