<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\CrudController;
use Illuminate\Http\Request;
// Load Models
use App\Models\Entities\Reference\Meeting;

/**
 * @Resource("meetings")
 * @Middleware("web")
 * @Middleware("auth")
 * @Middleware("xss")
 * @Middleware("timeout")
 * @Controller(prefix="reference")
 */
class MeetingController extends CrudController{

    protected $data = array();
    protected $view_source = "main.reference.meetings";
    protected $title = "Pertemuan";
    protected $route_name = "meetings";
    protected $model = Meeting::class;
    protected $script = 'app/js/meetings.js';

    /**
     * 
     * @Post("/meetings/datatable", as="meetings.datatable")
     */
    public function datatable(Request $request){
        return parent::datatable($request);
    }

    protected function create_validation(){
        return array(
            'date_meeting' => 'required',
            'title' => 'required|string',
            'body' => 'required|string',
            'address' => 'required|string',
        );
    }

    protected function edit_validation($id){
        return array(
            'date_meeting' => 'required',
            'title' => 'required|string',
            'body' => 'required|string',
            'address' => 'required|string',
        );
    }
    
}