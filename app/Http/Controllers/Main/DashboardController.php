<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\CrudController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route as RouteApp;
use App\Models\Traits\Authorizable;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;
use App\Helpers\CommonHelper;
// Load Models
use App\Models\Entities\Transaction\Dashboard;
use DB;

/**
 * @Resource("dashboards")
 * @Middleware("web")
 * @Middleware("auth")
 * @Middleware("xss")
 * @Middleware("timeout")
 */
class DashboardController extends CrudController{

    protected $data = array();
    protected $view_source = "main.dashboard";
    protected $title = "Dashboard";
    protected $model = Dashboard::class;
    protected $script = 'app/js/dashboards.js';

    public function index(){
        $month = (int) date("m");
        $this->data["semester"] = $month > 6 ? "Semester II" : "Semester I";
        $this->data["title"] = $this->title;
        $this->data["route"] = $this->route_name;
        $this->data["script"] = asset($this->script."?".time());
        return view($this->view_source, $this->data);
    }

    public function update($id, Request $request){
        return abort("404");
    }

    public function store(Request $request){
        return abort("404");
    }

    public function create(){
        return abort("404");
    }

    public function edit($id){
        return abort("404");
    }

    public function show($id){
        return abort("404");
    }

    public function destroy($id){
        return abort("404");
    }
    
}