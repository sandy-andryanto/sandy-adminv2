<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('instance')->index();
            $table->string('channel')->index();
            $table->string('level')->index();
            $table->string('level_name')->index();
            $table->text('message');
            $table->text('context');
            $table->integer('remote_addr')->nullable()->unsigned()->index();
            $table->string('user_agent')->nullable()->index();
            $table->integer('created_by')->nullable()->index();
            $table->dateTime('created_at')->index();
            $table->engine = 'InnoDB';
        });

        Schema::create('auth_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longtext('image')->nullable();
            $table->string('name')->nullable();
            $table->tinyInteger('gender')->default(0)->index();
            $table->string('city')->nullable();
            $table->longtext('address')->nullable();
            $table->string('username')->unique()->nullable()->index();
            $table->string('email')->unique()->index();
            $table->string('phone')->unique()->nullable()->index();
            $table->string('password')->index();
            $table->string('session_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->engine = 'InnoDB';
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
        Schema::dropIfExists('auth_users');
    }
}
