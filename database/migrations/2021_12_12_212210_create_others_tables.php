<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOthersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // notes by user
        Schema::create('ref_meetings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date_meeting')->index();
            $table->string('title')->index();
            $table->string('city')->nullable()->index();
            $table->string('state')->nullable()->index();
            $table->text('address')->nullable();
            $table->longText('body');
            $table->timestamps();
            $table->softDeletes();
            $table->engine = 'InnoDB';
        });

        // customer
        Schema::create('ref_customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable()->index();
            $table->string('email')->nullable()->index();
            $table->string('phone')->nullable()->index();
            $table->string('job_title')->nullable()->index();
            $table->string('company_name')->nullable()->index();
            $table->string('website')->nullable()->index();
            $table->string('city')->nullable()->index();
            $table->string('state')->nullable()->index();
            $table->string('zip_code')->nullable()->index();
            $table->text('street_address')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->engine = 'InnoDB';
        });

        // tasks
        Schema::create('ref_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date_first')->index();
            $table->date('date_last')->index();
            $table->string('title')->index();
            $table->longText('body');
            $table->integer('priority')->default(0)->index();
            $table->integer('status')->default(0)->index();
            $table->timestamps();
            $table->softDeletes();
            $table->engine = 'InnoDB';
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ref_meetings');
        Schema::drop('ref_customers');
        Schema::drop('ref_tasks');
    }
}
