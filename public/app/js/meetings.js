$(function(){

    if($("#table-data").length){
        var columns = [
            {
                "data": "id",
                "orderable": false,
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'date_meeting',
            },
            {
                data: 'title',
            },
            {
                data: 'address',
            },
            {
                data: 'action',
                "orderable": false,
                "className": "text-center"
            },
        ];
        setDataTable("#table-data", columns, "reference/meetings/datatable");
    }

});