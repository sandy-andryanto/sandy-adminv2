$(function(){

    if($("#table-data").length){
        var columns = [
            {
                "data": "id",
                "orderable": false,
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'subject',
            },
            {
                data: 'sort_content',
            },
            {
                data: 'readed_at',
                render: function (data, type, row, meta) {
                    return data !== null ? `<span class="badge badge-success">Sudah dibaca</span>` : `<span class="badge badge-warning">Belum dibaca</span>`;
                }
            },
            {
                data: 'action',
                "orderable": false,
                "className": "text-center"
            },
        ];
        setDataTable("#table-data", columns, "account/notifications/datatable");
    }

    

   

});