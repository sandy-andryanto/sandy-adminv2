$(function(){
    $("body").on("click", ".btn-artisan", function(e){
        e.preventDefault();
        var command = $(this).attr("data-value");
        ajax_setup();
        $.post(BASE_URL+"/configuration/settings/artisan", { "command" : command }, function(result){
            Swal.fire(
                'Berhasil!',
                'Command berhasil digenerate!',
                'success'
            )
            window.location.reload();
        });
        return false;
    });
});