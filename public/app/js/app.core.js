const BASE_URL 		= $('meta[name="base-url"]').attr('content');
const API_TOKEN     = $('meta[name="api-token"]').attr('content');
const CSRF_TOKEN 	= $('meta[name="csrf-token"]').attr('content');

$(document).ajaxComplete(function() {
	$('[data-toggle="tooltip"]').tooltip();
});

function elFinderBrowser (callback, value, meta) {
    tinymce.activeEditor.windowManager.openUrl({
        title: 'File Manager',
        url: $('meta[name="elfinder-url"]').attr('content'),
        /**
         * On message will be triggered by the child window
         * 
         * @param dialogApi
         * @param details
         * @see https://www.tiny.cloud/docs/ui-components/urldialog/#configurationoptions
         */
        onMessage: function (dialogApi, details) {
            if (details.mceAction === 'fileSelected') {
                const file = details.data.file;
                
                // Make file info
                const info = file.name;
                
                // Provide file and text for the link dialog
                if (meta.filetype === 'file') {
                    callback(file.url, {text: info, title: info});
                }
                
                // Provide image and alt text for the image dialog
                if (meta.filetype === 'image') {
                    callback(file.url, {alt: info});
                }
                
                // Provide alternative source and posted for the media dialog
                if (meta.filetype === 'media') {
                    callback(file.url);
                }
                
                dialogApi.close();
            }
        }
    });
}

function mask_money_format(input){
	if(input){
		return numeral(input).format('0,0').replace(/,/g, '.')
	}
	return 0;
}

function mask_money_raw(input){
	if(input){
		return parseFloat(input.replace(/\./g, ''));;
	}
	return 0;
}

function uuidv4() {
	return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
	  (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
	);
  }

function get_month_name(index){
	if(parseInt(index) === 1){
		return "Januari";
	}else if(parseInt(index) === 2){
		return "Februari";
	}else if(parseInt(index) === 3){
		return "Maret";
	}else if(parseInt(index) === 4){
		return "April";
	}else if(parseInt(index) === 5){
		return "Mei";
	}else if(parseInt(index) === 6){
		return "Juni";
	}else if(parseInt(index) === 7){
		return "Juli";
	}else if(parseInt(index) === 8){
		return "Agustus";
	}else if(parseInt(index) === 9){
		return "September";
	}else if(parseInt(index) === 10){
		return "Oktober";
	}else if(parseInt(index) === 11){
		return "November";
	}else if(parseInt(index) === 12){
		return "Desember";
	}else{
		return "undefined";
	}
}


function ajax_setup() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': CSRF_TOKEN,
            'Authorization': 'Bearer ' + API_TOKEN,
        }
    });
}

function formatMoney(number, decPlaces, decSep, thouSep) {
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSep = typeof decSep === "undefined" ? "." : decSep;
    thouSep = typeof thouSep === "undefined" ? "," : thouSep;
    var sign = number < 0 ? "-" : "";
    var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
    var j = (j = i.length) > 3 ? j % 3 : 0;

    return sign +
        (j ? i.substr(0, j) + thouSep : "") +
        i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
        (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}

function number_format(number, decimals, dec_point, thousands_sep) {
    // http://kevin.vanzonneveld.net
    // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +     bugfix by: Michael White (http://getsprink.com)
    // +     bugfix by: Benjamin Lupton
    // +     bugfix by: Allan Jensen (http://www.winternet.no)
    // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
    // +     bugfix by: Howard Yeend
    // +    revised by: Luke Smith (http://lucassmith.name)
    // +     bugfix by: Diogo Resende
    // +     bugfix by: Rival
    // +      input by: Kheang Hok Chin (http://www.distantia.ca/)
    // +   improved by: davook
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // +      input by: Jay Klehr
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // +      input by: Amir Habibi (http://www.residence-mixte.com/)
    // +     bugfix by: Brett Zamir (http://brett-zamir.me)
    // +   improved by: Theriault
    // +   improved by: Drew Noakes
    // *     example 1: number_format(1234.56);
    // *     returns 1: '1,235'
    // *     example 2: number_format(1234.56, 2, ',', ' ');
    // *     returns 2: '1 234,56'
    // *     example 3: number_format(1234.5678, 2, '.', '');
    // *     returns 3: '1234.57'
    // *     example 4: number_format(67, 2, ',', '.');
    // *     returns 4: '67,00'
    // *     example 5: number_format(1000);
    // *     returns 5: '1,000'
    // *     example 6: number_format(67.311, 2);
    // *     returns 6: '67.31'
    // *     example 7: number_format(1000.55, 1);
    // *     returns 7: '1,000.6'
    // *     example 8: number_format(67000, 5, ',', '.');
    // *     returns 8: '67.000,00000'
    // *     example 9: number_format(0.9, 0);
    // *     returns 9: '1'
    // *    example 10: number_format('1.20', 2);
    // *    returns 10: '1.20'
    // *    example 11: number_format('1.20', 4);
    // *    returns 11: '1.2000'
    // *    example 12: number_format('1.2000', 3);
    // *    returns 12: '1.200'
    var n = !isFinite(+number) ? 0 : +number, 
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        toFixedFix = function (n, prec) {
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            var k = Math.pow(10, prec);
            return Math.round(n * k) / k;
        },
        s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

function string_to_slug (str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
  
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}


function show_toast(option) {
	
}

function load_notification(){
	ajax_setup();
	$.post(BASE_URL + "/api/user/notifications/list",  function (result) {
		if(result){
			var unread = parseInt(result.unread);
			var list = result.list;
			var html  = ``;
			if(unread > 0){
				$("#notification-section").removeClass("d-none");
				$(".notification-count").text(unread);
				list.forEach(function(row){
					html += `
						<a href="`+BASE_URL+`/account/notifications/`+row.id+`" class="dropdown-item">
							<i class="fas fa-bell mr-2"></i> 
							`+(row.subject).substring(0, 28)+`...
						</a>
						<div class="dropdown-divider"></div>
					`;
				});
				$("#notification-list-section").html(html);
			}
		}
	});
}

function setDataTable(container, columns, url, additional){

	var oTable = $(container).DataTable({
		'processing': true,
		'serverSide': true,
		'ajax': {
			'url': BASE_URL+"/"+url,
			'type': 'POST',
			'headers': {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			"data": function (d) {
				let obj = JSON.parse('{ "uuid" :"' + uuidv4() + '"  }');
				if (additional &&additional.length > 0) {
					additional.forEach(function (row) {
						obj[row.key] = row.value;
					});
				}
				return $.extend({}, d, obj);
			}
		},
		'columns': columns,
		"order": [0, "DESC"],
		"initComplete": function (settings, json) {
			$(container+'_filter input').attr("placeholder", "Tekan Enter").addClass("form-control");
			$(container+'_filter input').unbind();
			$(container+'_filter input').bind('keyup', function (e) {
				if (e.keyCode == 13) {
					oTable.search(this.value).draw();
				}
			});
		},
		"language": {
			"sProcessing": "<i class='fas fa-sync fa-spin'></i>&nbsp;&nbsp;Sedang memuat data...",
			"sLengthMenu": "Tampilkan _MENU_ entri",
			"sZeroRecords": "Tidak ditemukan data yang sesuai",
			"sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
			"sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
			"sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
			"sInfoPostFix": "",
			"sSearch": "Cari:",
			"sUrl": "",
			"oPaginate": {
				"sFirst": "Pertama",
				"sPrevious": "Sebelumnya",
				"sNext": "Selanjutnya",
				"sLast": "Terakhir"
			}
		},
		"autoWidth": false,
      	"responsive": false,
		"destroy": true,
	});

    $("body").on("click", ".btn-delete", function(e){
        e.preventDefault();
        var url_delete = $(this).attr('href');
		var id = $(this).attr("data-id");
		var token = CSRF_TOKEN;
		Swal.fire({
			title: 'Konfirmasi',
			text: "Apakah anda yakin ingin menghapus data ini ?",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
			showLoaderOnConfirm: true,
		  }).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url : url_delete,
					type: 'DELETE',
					data: { "id" : id, "_token" : token },
					success: function(){
						Swal.fire(
							'Berhasil!',
							'Data berhasil dihapus!',
							'success'
						)
						load_notification();
						$(container).DataTable().ajax.reload();
					}
				});
			}
		  });
        return false;
    });
}

$(function () {

	moment.locale('id');

	load_notification();
    
    $('[data-toggle="tooltip"]').tooltip();

	$("body").on("click", "#btn-delete", function(e){
        e.preventDefault();
        var url_delete = $(this).attr('href');
		var id = $(this).attr("data-id");
        var redirect = $(this).attr("data-redirect");
		var token = CSRF_TOKEN;	
		Swal.fire({
			title: 'Konfirmasi',
			text: "Apakah anda yakin ingin menghapus data ini ?",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
			showLoaderOnConfirm: true,
		  }).then((result) => {
			 if (result.isConfirmed) {
				$.ajax({
					url : url_delete,
					type: 'DELETE',
					data: { "id" : id, "_token" : token },
					success: function(){
						Swal.fire(
							'Berhasil!',
							'Data berhasil dihapus!',
							'success'
						)
						setTimeout(function(){ 
							window.location.href = redirect;
						}, 3000);
						
					}
				});}
		  });
        return false;
    });

	if ($("#form-submit,.form-submit").length) {
		$("#form-submit,.form-submit").submit(function (e) {
			e.preventDefault();
			let form = this;
			Swal.fire({
				title: 'Konfirmasi',
				text: "Apakah anda yakin isian form ini sudah benar ?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				showLoaderOnConfirm: true,
			  }).then((result) => {
				if (result.isConfirmed) {
					$(form).unbind('submit').submit();
				}
			  });
			return false;
		});
	}

	if ($(".select2").length > 0) {
		$(".select2").select2();
	}

	if ($(".input-datepicker").length) {
		$(".input-datepicker").daterangepicker({
			singleDatePicker: true,
			drops: 'top',
			autoApply: true,
			autoUpdateInput: true,
			showDropdowns: true,
			locale: {
				format: 'YYYY-MM-DD',
			}
		});
	}

	if ($(".input-datetimepicker").length) {
		$(".input-datetimepicker").daterangepicker({
			singleDatePicker: true,
			drops: 'top',
			autoApply: true,
			autoUpdateInput: true,
			showDropdowns: true,
			timePicker: true,
			timePicker24Hour: true,
			locale: {
				format: 'YYYY-MM-DD HH:mm',
			}
		});
	}

	if ($(".file-input").length) {
		$(".file-input").fileinput({
			showPreview: false,
			showRemove: true,
			showUpload: false,
			showUploadStats: true,
		});
	}

	if ($(".file-input-image").length) {
		if ($(".file-input-image-preview").length) {
			var img_prview = new Array();
			$(".file-input-image-preview").each(function(){
				var imageUrl = $(this).val();
				img_prview.push(imageUrl);
			});	
			img_prview.forEach(function(row, i){
				var temp = row;
				console.log(row, i);
				$(".file-input-image:eq("+i+")").fileinput({
					initialPreview: [temp],
					initialPreviewAsData: true,
					showUpload: false,
					allowedFileExtensions: ["jpg", "png", "gif"],
					showRemove: false,
					maxFileCount: 1,
					removeLabel: '',
				});
			});
		} else {
			$(".file-input-image").fileinput({
				showUpload: false,
				showRemove: false,
				allowedFileExtensions: ["jpg", "png", "gif"],
				initialPreviewAsData: true,
				maxFileCount: 1,
			});
		}
	}

	
    if($(".number_format").length){
		$(".number_format").each(function(e){
			var num = parseFloat($(this).text());
			var num_result = number_format(num, 2);
			$(this).text(num_result);
		});
	}

	if($(".input_number_format").length){
		$(".input_number_format").each(function(e){
			var num = parseFloat($(this).val());
			var num_result = number_format(num, 2);
			$(this).val(num_result);
		});
	}

	if($(".input-money").length){
		$('.input-money').maskMoney({prefix:'', thousands:'.', decimal:',', precision:0});
	}

	var parent_id = -1;
	$("a.nav-link").each(function(){
		if($(this).hasClass("active")){
			parent_id = $(this).attr("data-parent");
		}
	});

	if(parseInt(parent_id) > 0){
		//$("li.nav-item[data-id="+parent_id+"]").addClass("active");
		$("li.nav-item[data-id="+parent_id+"]").addClass("menu-open").children("a").addClass("active");
    }

	if($(".input-tinymce").length){
		$(".input-tinymce").each(function(){
			let id = $(this).attr("id");
			tinymce.init({
				file_picker_callback : elFinderBrowser,
				selector: 'textarea#'+id,
				height : "480",
				menubar: false,
				plugins: "link image code media imagetools",
				toolbar: 'undo redo | styleselect | forecolor | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | link image media | code'
			});
		});
	}
	
	$(".sidebar").removeClass("d-none");

	
});