$(function(){

    if($("#table-data").length){
        var columns = [
            {
                "data": "id",
                "orderable": false,
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'name',
            },
            {
                data: 'email',
            },
            {
                data: 'website',
            },
            {
                data: 'phone',
            },
            {
                data: 'street_address',
            },
            {
                data: 'action',
                "orderable": false,
                "className": "text-center"
            },
        ];
        setDataTable("#table-data", columns, "reference/customers/datatable");
    }

    

   

});