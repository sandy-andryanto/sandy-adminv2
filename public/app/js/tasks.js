$(function(){

    if($("#table-data").length){
        var columns = [
            {
                "data": "id",
                "orderable": false,
                "className": "text-center",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'date_first',
            },
            {
                data: 'date_last',
            },
            {
                data: 'title',
            },
            {
                data: 'priority',
            },
            {
                data: 'status',
                render: function (data, type, row, meta) {
                   if(parseInt(data) === 0){
                     return `<span class="badge badge-info">Memulai</span>`;
                  }else if(parseInt(data) === 1){
                    return `<span class="badge badge-warning">Proses</span>`; 
                  }else if(parseInt(data) === 2){
                    return `<span class="badge badge-success">Selesai</span>`;
                  }else{
                    return `<span class="badge badge-danger">Undefined</span>`;
                  }
                }
            },
            {
                data: 'action',
                "orderable": false,
                "className": "text-center"
            },
        ];
        setDataTable("#table-data", columns, "reference/tasks/datatable");
    }

    

   

});